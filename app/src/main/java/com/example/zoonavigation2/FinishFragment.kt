package com.example.zoonavigation2

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zoonavigation2.databinding.FragmentFinishBinding

class FinishFragment : Fragment() {

    private var _binding: FragmentFinishBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFinishBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var prev = FinishFragmentDirections.actionFinishFragmentToSecondFragment()

        binding.buttonPrev.setOnClickListener {
            findNavController().navigate(prev)
        }
        binding.buttonFinish.setOnClickListener {

            val intent = Intent(this@FinishFragment.context, ZooActivity::class.java)
            intent.putExtra("zooName", "Best Zoo Ever")
            startActivity(intent)
            activity?.finish()
        }
    }
}