package com.example.zoonavigation2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zoonavigation2.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btn1.setOnClickListener {
            navigate(getString(R.string.ostrich_name), getString(R.string.ostrich_fact))

        }
        binding.btn2.setOnClickListener {
            navigate(getString(R.string.lion_name), getString(R.string.lion_fact))

        }
        binding.btn3.setOnClickListener {
            navigate(getString(R.string.buffalo_name), getString(R.string.buffalo_fact))
        }
        binding.btn4.setOnClickListener {
            navigate(getString(R.string.tapir_name), getString(R.string.tapir_fact))
        }
    }

    fun navigate(name: String, fact: String) {
        val page = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(name, fact)
        findNavController().navigate(page)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}